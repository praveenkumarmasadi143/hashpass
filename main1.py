from sys import api_version
from fastapi import FastAPI
from yt_dlp import YoutubeDL
from typing import Dict, Optional
from pydantic import BaseModel

app = FastAPI()


class Post(BaseModel):
    post_id: int
    author: str


@app.get("/")
async def hello():
    return {"message": "Hi praveen this is ashok how are "}

@app.get("/items/{item_id}/")#####path
async def read_item(item_id: str):
    return {"item_id": item_id}

@app.get("/items")#####query
async def read_item(item_id: str):
    return {"item_id": item_id}

@app.get("/data")
async def show_data(data_id:str,format: Optional[str]=None):
    api_url="https://"+data_id
    if format:
        api={"format":format}
    else:
        api={
            "format":"",
            }
    with FastAPI(api) as ap:
        api.dowload([api_url])
    return {"data_id":data_id}
    

@app.get("/watch")
async def download_video(v: str, format: Optional[str] = None) -> Dict:
    youtube_url = "https://www.youtube.com/watch?v=" + v
    if format:
        ydl_opts = {"format": format}
    else:
        ydl_opts = {
            'format': 'best/bestvideo+bestaudio',
        }
    with YoutubeDL(ydl_opts) as ydl:
        ydl.download([youtube_url])
    return {"video_id": v}


@app.post("/get_post/")
async def get_post(post_info: Post):
    return {
        "get_post_id": post_info.post_id,
        "get_post_author": post_info.author
    }

