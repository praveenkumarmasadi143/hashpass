# import requests 
# import sqlalchemy 
# import json
# import fastapi
# import time
#import pandas as pd

# req=r.get("https://api.coingecko.com/api/v3/coins/bitcoin")
# #,params={"ids":"bitcoin","vs_currencies":"usd,eur,inr"})
# #coins_list=req.json()
# json_data=req.json()

# with open("btc_coin.json", "w") as file:
#     json.dump(json_data,file,indent=4)



# def get_crypto_price(symbol):
#     api_key="/api/v3"
#     api_url="https://api.coingecko.com/api/v3/coins/bitcoin"
#     raw=r.get(api_url).json()
#     price=json_data["market_data"]["price_change_24h_in_currency"]["inr"],"%"
#     return float(price)
# btc=get_crypto_price('btc')
# print('price changes in 24hrs: {} inr'.format(btc))


# def price_changes():
#     value="price changes in 24hrs ",json_data["market_data"]["price_change_24h_in_currency"]["inr"],"%"
#     return value

from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.future import Engine
from sqlalchemy import Column,String,Integer,Float
import requests
import json
import


engine: Engine= create_engine("sqlite:///response.db")
Base=declarative_base()
Session=sessionmaker(bind=engine)
session=Session()

class Responses(Base):
    __tablename__="responses"
    row_no : int = Column(Integer,primary_key=True)
    #    username : str =Column(String(64),primary_key=True)
    ids : str = Column(String(64),unique=False)
    vs_currency : str = Column(String(64))
    curent_value : float = Column(Float)
    price_change: float = Column(Float)
    
    def simple_price(self):
        res=requests.get("https://api.coingecko.com//api/v3/coins",params={"ids":self.ids})
        data=res.json()[0]
        self.curent_value=data["market_data"]["current_price"][self.vs_currency]
        

    # def price_change_(self,time):
    #     resp=requests.get("https://api.coingecko.com//api/v3/coins",params={"vs_currency":self.vs_currency,"ids":self.ids,"price_change_percentage":time})
    #     jason_list=resp.json()[0]
    #     self.price_change=jason_list[f'price_change_percentage_{time}_in_currency']
        



# Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)