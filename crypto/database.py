from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
#Create a database URL for SQLAlchemy
SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"

#we are "connecting" to a SQLite database (opening a file with the SQLite database).
# The file will be located at the same directory in the file sql_app.db.
# That's why the last part is ./sql_app.db

# Create the SQLAlchemy engine
# The first step is to create a SQLAlchemy "engine".
# We will later use this engine in other places.
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)

#Create a SessionLocal class
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

#Create a Base class
#Now we will use the function declarative_base() that returns a class.

Base = declarative_base()