from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
import os
import json
import hashlib

class Post(BaseModel):
    id:str
    description:str
    content:str


class Users(BaseModel):
    username: str 
    hash_algorithm:str
    no_of_iterations: int 
    email: str 

class Sign(BaseModel):
    username:str
    password:str
    



app=FastAPI()

data=[]
def default(self,obj):
    obj=self.dict()
    return json.JSONEncoder.default(obj)

@app.get("/")
def hello(a:str,b:int | None=None):
    return {
        "a":a,"b":b
        }
    

@app.post("/post/add")
def add_post(post:Post):
    data.append(post.dict())
    return{
        "message":"sucessfully added to database",
        "post_added":{
            "id":post.id,
            "description":post.description,
            "content":post.content
        }
    }

@app.post("/singup")
def sign_up(user:Users):
    data.append(user.dict())
    return{
        "message":"add suessfully",
        "added":{
            "username":user.user,
            "hash_alogrithm":user.hash_alogrithm,
            "no_of_iterations":user.no_of_iterations,
            "email":user.email

        }
    }


def hash_password(self, password: str):
        self.salt = os.urandom(64).hex()
        self.hash_algorithm = "pbkdf2-hmac-sha512"
        self.no_of_iterations = 100000
        self.hashed_password = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()


def authenticate(self, password: str):
    if self.hash_algorithm == "pbkdf2-hmac-sha512":
        new_hash = hashlib.pbkdf2_hmac(self.hash_algorithm.split("-")[-1], password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations)
        if new_hash == self.hashed_password:
            return True
        else:
            return False



def get_all_posts():
    return{
        "results":data
    }


@app.post("/post/delete/{post_id}")
def delete_post(post_id:str):
    for value in data:
        if value['id']==post_id:
            data.remove(value)



# @app.post("/login")
# def login(sign:Sign):
#     file_to_load="data.json"
#     if os.path.exists(file_to_load):
#         with open('data.json','W') as data_file:
#             global data
#             data = json.load(data_file)

@app.post("/login/")
async def login(sign:Sign):
    with open("data.json","r") as file:
        =json.load(file)
    with open("data.json","w") as file:
        entry={}
        entry["username"]=sign.username
        entry["password"]=sign.password
    return {
        "username":sign.username
        
        }


@app.on_event("startup")
def load_data():
    file_to_load="data.json"
    if os.path.exists(file_to_load):
        with open(file_to_load,'r') as file:
            global data
            data=json.load(file)



@app.on_event("shutdown")
def save_data():
    with open("data.json","w") as file:
        json.dump(data,file)

