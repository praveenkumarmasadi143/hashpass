from datetime import datetime

from sqlalchemy.orm import sessionmaker
from sqlalchemy.future import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String,Float,DateTime,create_engine
#import os

# BASE_DIR=os.path.dirname(os.path.realpath(__file__))
#connection_string="sqlite:///"+os.path.join(BASE_DIR,'site.db')
engine:Engine =create_engine("sqlite:///cryptocurr.db",echo=True)

Base=declarative_base()
Session=sessionmaker()
session=Session()

class User(Base):
    __tablename__='cryptocurr'
    id=Column(Integer(),primary_key=True)
    ids=Column(String())
    vs_currency : str = Column(String(64))
    curent_value : float = Column(Float(),nullable=True)
    price_change: float = Column(Float(),nullable=True)
    #date_created =Column(DateTime(),default=datetime.utcnow)

    def __repr__(self):
        return f"<user ids={self.ids} vs_currency={self.vs_currency}>"
def

newuser=User(id=1,ids="1",vs_currency="2")
print(newuser)

#Base.metadata.create_all(engine)
       