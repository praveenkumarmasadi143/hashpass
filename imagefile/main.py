from http.client import responses
import os
from fastapi import FastAPI
from fastapi.responses import FileResponse,StreamingResponse

app=FastAPI()

path=r"\Users\pmasadi\webapi"
video_path=r"C:\Users\pmasadi\webapi\imagefile\sarega.mp4"

@app.get("/")
def index():
    return{"Hello":"world"}

@app.get("/bird",responses={200:{"description":"A picture of bird.","content":"image/jpeg"}})
def bird():
    file_path=os.path.join(path,"imagefile/bird.jpg")
    if os.path.exists(file_path):
        return FileResponse(file_path,media_type="image/jpeg")
    return {"error":"file not found!"}


@app.get("/video")
def video():
    file_like = open(video_path, mode="rb")
    return StreamingResponse(file_like, media_type="video/mp4")
    
    
    