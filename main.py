from fastapi import FastAPI
from hashpass import DBSession, Items,Users
from sqlalchemy import select
from pydantic import BaseModel


app = FastAPI()


class Item(BaseModel):
    item_name: str
    item_description: str
    item_image_url: str


class User(BaseModel):
    username:str
    password: str
    email:str


@app.get("/item/{item_id}")
async def get_item(item_id: int, item_name: str | None = None):
    async with DBSession() as session:
        async with session.begin():
            sql_stmt = select(Items).where(Items.id == item_id)
            print(sql_stmt)
            result = await session.stream_scalars(sql_stmt)
            result: Items = await result.first()
    return {
        "item_name": result.item_name,
        "item_description": result.item_description,
        "item_image_url": result.item_image_url
    }


################
@app.get("/User/{User_id}")
async def get_Users(User_id: int, Users_name: str | None = None):
    async with DBSession() as session:
        async with session.begin():
            sql_stmt = select(Users).where(Users.id == User_id)
            print(sql_stmt)
            result = await session.stream_scalars(sql_stmt)
            result: Users = await result.first()
    return {
        "username": result.username,
        "hash_algorithm": result.hash_algorithm,
        "no_of_iterations": result.no_of_iterations,
        "salt":result.salt,
        " hashed_password": result.hashed_password,
        "email":result.email

    }


#############
@app.post("/item/add")
async def add_item(item: Item):
    async with DBSession() as session:
        async with session.begin():
            try:
                item_to_add = Items(**item.dict())
                session.add(item_to_add)
                await session.commit()
                return {
                    "message": "item added",
                    "added_item": item.dict()
                }
            except ValueError as err:
                return {
                    "message": str(err),
                }
########
@app.post("/User/add")
async def add_Users(user: User):
    async with DBSession() as session:
        async with session.begin():
            try:
                User_to_add = Users(username=user.username)
            

                session.add(User_to_add)
                
                await session.commit()
                return {
                    "message": "User added",
                    "added_item": User.dict()
                }
            except ValueError as err:
                return {
                    "message": str(err),
                }

